# README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* This repository holds a library to generate objects of a given class from a data file.

### How do I get set up? ###

* Simply import the library to an existing project to start using it. Further instructions will be given as the project progresses.
